﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex22
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Task A ///

            int x;
            var movies = new string[5]{"Avartar", "Finding Dory", "The Hunger Games", "Here I stay", "Divergent" };
            for(x = 0; x<movies.Length;x++)
            {
                Console.WriteLine($"Movies {x+1} : {movies[x]}");
            }

            /// Task B /// 

            movies[0] = "Star Wars";
            movies[2] = "Legally Blonde";

            /// Task C ///

            Array.Sort(movies);

            /// Task D ///

            Console.WriteLine("");

            Console.WriteLine($"Length of the movies : {movies.Length}");

            /// Task E ///
            Console.WriteLine("");

            for(x=0;x<movies.Length;x++)
            {
                Console.WriteLine($"The title of the movies {movies[x]}");
            }

            /// Task F ///

            //List<T> : restaurants = new List<string>();
            var restaurants = new List<string>();   //= new List<T>();

            restaurants.Add("Harbourside");
            restaurants.Add("Mc Donalds");
            restaurants.Add("Smart India");
            restaurants.Add("Love Rosies");
            restaurants.Add("The Barrio Brothers");

             /// Task G ///

            Console.WriteLine("");
            Console.WriteLine("List of Places to eat in Tauranga: ");
             //Array.Sort(restaurants);
             restaurants.Sort();
             Console.WriteLine(string.Join(",", restaurants));

             /// Task H ///

             Console.WriteLine("");
             restaurants.Remove("Smart India");
             Console.WriteLine(string.Join(",", restaurants));
             Console.WriteLine("");
             Console.WriteLine("");
        }
    }
}
